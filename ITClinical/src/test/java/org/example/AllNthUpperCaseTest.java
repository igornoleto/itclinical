package org.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AllNthUpperCaseTest {

    @Test
    public void testAllNthUpperCase() {

        String testingValue = "ITCLiNicAl";

        AllNthUpperCase service = new AllNthUpperCase(1);
        assertEquals("ITCLNA", service.getNthUpperCase(testingValue));

        service = new AllNthUpperCase(2);
        assertEquals("TLN", service.getNthUpperCase(testingValue));

        service = new AllNthUpperCase(3);
        assertEquals("CNA", service.getNthUpperCase(testingValue));

        service = new AllNthUpperCase(100);
        assertEquals("", service.getNthUpperCase(testingValue));

        service = new AllNthUpperCase(-1);
        assertEquals("", service.getNthUpperCase(testingValue));
    }

    @Test
    public void testAllNthCharacters() {

        String testingValue= "!ITCL1NicAl";

        AllNthUpperCase service = new AllNthUpperCase(1);
        assertEquals("!ITCL1NA", service.getNthCharacters(testingValue));

        service = new AllNthUpperCase(2);
        assertEquals("IC1A", service.getNthCharacters(testingValue));

        service = new AllNthUpperCase(3);
        assertEquals("T1", service.getNthCharacters(testingValue));

        service = new AllNthUpperCase(100);
        assertEquals("", service.getNthCharacters(testingValue));

        service = new AllNthUpperCase(-1);
        assertEquals("", service.getNthCharacters(testingValue));
    }

    @Test
    public void testAllNthUpperCaseCount() {

        String testingValue = "ITCLINicAl";

        AllNthUpperCase service = new AllNthUpperCase(1);
        assertEquals("ITCLINA", service.getNthUpperCaseCount(testingValue));

        service = new AllNthUpperCase(2);
        assertEquals("TLN", service.getNthUpperCase(testingValue));

        service = new AllNthUpperCase(3);
        assertEquals("CNA", service.getNthUpperCase(testingValue));

        service = new AllNthUpperCase(100);
        assertEquals("", service.getNthUpperCase(testingValue));

        service = new AllNthUpperCase(-1);
        assertEquals("", service.getNthUpperCase(testingValue));
    }

}