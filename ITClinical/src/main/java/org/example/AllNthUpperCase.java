package org.example;

import java.util.HashMap;
import java.util.Map;

public class AllNthUpperCase {

    private int number;

    public AllNthUpperCase(int number) {
        this.number = number;
    }

    public String getNthUpperCase(String text) {
        if (number <= 0 || text.length() < number) {
            return "";
        }
        StringBuilder finalText = new StringBuilder();

        int value = 0;

        for (int i = 0; i < text.length(); i++) {
            value++;
            char l = text.charAt(i);
            if (Character.isUpperCase(l) && value % number == 0) {

                finalText.append(l);
            }
        }
        return finalText.toString();
    }

    public String getNthCharacters(String text) {
        if (number <= 0 || text.length() < number) {
            return "";
        }
        StringBuilder finalText = new StringBuilder();

        int value = 0;

        for (int i = 0; i < text.length(); i++) {
            value++;
            char l = text.charAt(i);
            if (!Character.isLetterOrDigit(l) || Character.isUpperCase(l) || Character.isDigit(l)) {
                if (value % number == 0) {
                    finalText.append(l);
                }

            }
        }
        return finalText.toString();
    }

    public String getNthUpperCaseCount(String text) {
        if (number <= 0 || text.length() < number) {
            return "";
        }
        StringBuilder finalText = new StringBuilder();

        HashMap<Character, Integer> finalMap = new HashMap<>();

        int value = 0;

        for (int i = 0; i < text.length(); i++) {
            value++;
            char l = text.charAt(i);
            if (Character.isUpperCase(l) && value % number == 0) {
                finalText.append(l);
                if (finalMap.get(l) != null) {
                    finalMap.put(l, finalMap.get(l) + 1);
                } else {
                    finalMap.put(l, 1);
                }
            }
        }
        finalMap.forEach((key, mapValue) -> System.out.println(key + " = " + mapValue));
        return finalText.toString();
    }

}